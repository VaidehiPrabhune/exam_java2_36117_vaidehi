package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java2ExamBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Java2ExamBootApplication.class, args);
	}

}
