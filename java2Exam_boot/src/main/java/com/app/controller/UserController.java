package com.app.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Role;
import com.app.pojos.Teacher;
import com.app.service.ITeacherService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private ITeacherService teacherService;

	public UserController() {
		System.out.println("In constructor of " + getClass().getName()); // null
	}

	@GetMapping("/login")
	public String showLoginForm() {
		System.out.println("In UserController's showLoginForm()");

		return "/user/login"; // actual view name : /WEB-INF/views/user/login.jsp
	}

	@PostMapping("/login")
	public String processLoginForm(@RequestParam String email, @RequestParam String password, Model map,
			HttpSession session, RedirectAttributes flashMap) {
		System.out.println("In UserController's processLoginForm() " + email + " " + password);

		try {
			Teacher validatedUser = teacherService.authenticateUser(email, password);

			// add validated user details under session scope
			session.setAttribute("user_details", validatedUser);

			flashMap.addFlashAttribute("message", " Login successfull " + validatedUser.getUserRole());

			// check the role and redirect accordingly
			if (validatedUser.getUserRole().equals(Role.ADMIN)) {
				return "redirect:/admin/list";

			}

			return "redirect:/teacher/details";

		} catch (RuntimeException e) {
			System.out.println("Error in processLoginForm() " + e); // NoResultException

			// in case of failure forward client to login form with error message
			map.addAttribute("message", "Invalid Login or password, Please retry.....");
		}

		return "/user/login"; // actual view name : /WEB-INF/views/user/login.jsp
	}
	
	
}
