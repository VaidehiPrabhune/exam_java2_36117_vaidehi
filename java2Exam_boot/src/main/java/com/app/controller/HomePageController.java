package com.app.controller;

import java.time.LocalDateTime;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller	//mandatory
public class HomePageController {
	public HomePageController() 
	{
		System.out.println("In constructor of " + getClass().getName());
	}
	
	
	@RequestMapping("/")
	public String showHomePage( Model map )
	{
		System.out.println("In HomePageController's showHomePage() ");
		
		map.addAttribute("server_timestamp", LocalDateTime.now());
		
		return "/index";	
		
		
	}
}
