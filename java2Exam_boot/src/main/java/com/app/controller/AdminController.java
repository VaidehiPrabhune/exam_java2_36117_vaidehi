package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Role;
import com.app.pojos.Teacher;
import com.app.service.ITeacherService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired	
	private ITeacherService teacherService;
	
	public AdminController() 
	{
		System.out.println("In constructor of " + getClass().getName());
	}
	
	@GetMapping("/list")
	public String showTeacherList(Model map)
	{
		System.out.println("In AdminController's showTeacherList() ");
		
		List<Teacher> list = teacherService.listAllTeacher();
		
		//save current vendor list under Model map --> auto saved under req scope
		map.addAttribute("teacher_list", list);
		return "/admin/list";	//actual view name ret by V.R : /WEB-INF/views/admin/list.jsp
	}
	
	@RequestMapping("/delete")
	public String removeTeacherDetails(@RequestParam int id, RedirectAttributes flashmap)
	{
		System.out.println("In AdminController's removeteacherDetails() witj vendorId " + id);
		
		String message = teacherService.deleteTeacherDetails(id);
		flashmap.addFlashAttribute("message", message);
		
		return "redirect:/admin/list";
	}
	
	@GetMapping("/update")
	public String showUpdateForm(@RequestParam int id, Model map)
	{
		System.out.println("In TeacherController's showUpdatePage() ");
		
		Teacher t = teacherService.editTeacherDetails(id);
		
		t.setId(id);
		map.addAttribute("teacher", t);
		
		System.out.println("-------" + t);
		
		return "/teacher/update";
	}
	
	@PostMapping("/update")
	public String processUpdateForm(@ModelAttribute("teacher") Teacher t, RedirectAttributes flashmap)
	{
		System.out.println("-------" + t);
		t.setUserRole(Role.TEACHER);		
		teacherService.registerTeacher(t);
		String message = teacherService.registerTeacher(t);
		flashmap.addFlashAttribute("message", message);
		
		return "redirect:/admin/list";	
	}
	
}
