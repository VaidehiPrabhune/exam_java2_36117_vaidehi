package com.app.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Role;
import com.app.pojos.Subject;
import com.app.pojos.Teacher;
import com.app.service.ITeacherService;

@Controller
@RequestMapping("/teacher")
public class TeacherController {
	
	@Autowired	
	private ITeacherService teacherService;
	
	public TeacherController() {
		System.out.println("in teacher controller");
	}
	
	@GetMapping("/details")
	public String showTeacherDetails(Model map)
	{
		System.out.println("In teacherController  ");							
		return "/teacher/details";	
	}
	
	@GetMapping("/update")
	public String showUpdateForm(@RequestParam int id, Model map)
	{
		System.out.println("In TeacherController's showUpdatePage() ");
		
		Teacher t = teacherService.editTeacherDetails(id);
		
		t.setId(id);
		map.addAttribute("teacher", t);
		
		System.out.println("-------" + t);
		
		return "/teacher/update";
	}
	
	@PostMapping("/update")
	public String processUpdateForm(@ModelAttribute("teacher") Teacher t, RedirectAttributes flashmap)
	{
		System.out.println("-------" + t);
		t.setUserRole(Role.TEACHER);
		teacherService.registerTeacher(t);
		
		String message = teacherService.registerTeacher(t);
		flashmap.addFlashAttribute("message", message);
		
		return "redirect:/teacher/details";	
	}
	
	
	@GetMapping("/Subjectregister")
	public String showRegForm(Subject s)
	{
		System.out.println("In teacherController's showRegForm() ");				
		return "/teacher/Subregister";	
	}
	
	@PostMapping("/Subjectregister")
	public String processRegForm(/*@ModelAttribute("vendor")*/ @Valid Subject s, BindingResult result, 
									RedirectAttributes flashmap)	
	{	
		System.out.println("In teacherController's processRegForm() " + s);	//v : transient
						
		//check for presentation logic validation
		if(result.hasErrors())	//returns true if found errors
		{
			System.out.println("P.L. error in Data Binding " + result);
						
			return "/teacher/Subregister";
		}
		
		System.out.println("No P.L. errors... Continue with B.L...");
		teacherService.addSubject(s);	
		
		flashmap.addFlashAttribute("message", "subject added successfully...");
		return "redirect:/teacher/details";	
	}
	
}
