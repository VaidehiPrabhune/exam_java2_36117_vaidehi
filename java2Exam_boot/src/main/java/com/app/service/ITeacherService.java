package com.app.service;

import java.util.List;

import com.app.pojos.Subject;
import com.app.pojos.Teacher;

public interface ITeacherService {
	public Teacher authenticateUser(String email, String password);
	
	public Teacher editTeacherDetails(int id);
	
	public String registerTeacher(Teacher teacher);
	
	void addSubject(Subject a);
	
	List<Teacher> listAllTeacher();
	
	public String deleteTeacherDetails(int id);
}
