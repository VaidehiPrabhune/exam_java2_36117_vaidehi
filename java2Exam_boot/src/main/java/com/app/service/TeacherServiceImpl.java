package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.ITeacherDao;
import com.app.pojos.Subject;
import com.app.pojos.Teacher;

@Service		
@Transactional	
public class TeacherServiceImpl implements ITeacherService {

	@Autowired
	private ITeacherDao teacherDao;
	
	@Override
	public Teacher authenticateUser(String email, String password) {
		return teacherDao.authenticateUser(email, password);
	}

	@Override
	public Teacher editTeacherDetails(int id) {
		
		return teacherDao.getTeacherDetails(id);
	}

	@Override
	public String registerTeacher(Teacher teacher) {
		return teacherDao.registernewTeacher(teacher);
	}

	@Override
	public void addSubject(Subject a) {
		teacherDao.addSubject(a);
	}

	@Override
	public List<Teacher> listAllTeacher() {
		return teacherDao.listAllTeacher();
	}

	@Override
	public String deleteTeacherDetails(int id) {
		return teacherDao.deleteTeacherDetails(id);
	}

	

}
