package com.app.dao;

import java.util.List;

import com.app.pojos.Subject;
import com.app.pojos.Teacher;

public interface ITeacherDao {
	//Teacher Login 
	Teacher authenticateUser(String email,String password);
	
	//get teacher details by id
	Teacher getTeacherDetails(int id);
	
	//register new teacher
	String registernewTeacher(Teacher teacher);	//input --> transient pojo
	
	//add subject
	void addSubject(Subject a);
	
	//get all teachers
	List<Teacher> listAllTeacher();
	
	//remove teacher
	public String deleteTeacherDetails(int id);
	
}
