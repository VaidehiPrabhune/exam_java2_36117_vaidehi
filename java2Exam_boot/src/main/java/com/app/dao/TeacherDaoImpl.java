package com.app.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Role;
import com.app.pojos.Subject;
import com.app.pojos.Teacher;

@Repository	
public class TeacherDaoImpl implements ITeacherDao {

	@Autowired
	private EntityManager manager;
	
	@Override
	public Teacher authenticateUser(String email, String password) {
		String jpql = "select t from Teacher t  where t.email=:em and t.password=:pass";
		
		Teacher t = manager.createQuery(jpql, Teacher.class).setParameter("em", email)
				   .setParameter("pass", password).getSingleResult();

		return t;	//dao layer is returning PERSISTENT vendor to Service
	}

	@Override
	public Teacher getTeacherDetails(int id) {
		Teacher t = manager.find(Teacher.class, id);
		
		return t;
	}

	@Override
	public String registernewTeacher(Teacher teacher) {
		manager.merge(teacher);
		
		return "Teacher registered Successfully..!!";
	}

	@Override
	public void addSubject(Subject a) {
		addSubject(a);
		
	}

	@Override
	public List<Teacher> listAllTeacher() {
		String jpql = "select t from Teacher t  where t.userRole = :rl";
		
		List<Teacher> list = manager.createQuery(jpql, Teacher.class).setParameter("rl", Role.TEACHER).getResultList();
		
		return list;
	}

	@Override
	public String deleteTeacherDetails(int id) {
		
		String message = "Teacher Deletion failed..!!";
		//use Entity Managers : public void remove(Object persistentPojoRef)
		
		//get persistent pojo ref from its id --> public T find(T class, Object uniqueId)
		Teacher t = manager.find(Teacher.class, id);
		
		if(t != null)
		{
			//v : PERSISTENT
			manager.remove(t);
			
			message = "Teacher Deleted Successfully..!!";
		}

		return message;
	}

}
