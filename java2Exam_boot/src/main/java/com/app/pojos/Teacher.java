package com.app.pojos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

//tb_teacher
//t_id(PK),t_name,t_password,t_email,t_dob,t_roll.
@Entity
@Table(name = "tb_teacher")
public class Teacher {
	@Id //PK 
	@GeneratedValue(strategy = GenerationType.IDENTITY) //strategy = AUTO will be replaced : auto_increment
	@Column(name = "t_id")
	private int id;		
	
	@NotBlank(message = "Name can not be empty")
	@Column(name="t_name")
	private String name;	
	
	@Column(length = 30,name="t_password")
	private String password;	
	
	@Column(length = 30,unique = true,name="t_email")
	@NotBlank(message = "Email can not be empty")
	@Email(message = "Blank or Invalid Email")
	private String email;		
	
	@Column(name = "t_dob")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Past
	private LocalDate dob;	
	
	@Column(name = "t_roll")
	private int rollno;	
	
	@Enumerated(EnumType.STRING)
	@Column(name="user_role",length = 20)
	private Role userRole;
	
	@OneToMany(mappedBy = "subjectTeacher",cascade = CascadeType.ALL,orphanRemoval = true)
	private List<Subject> subjects=new ArrayList<>();
	
	
	public Teacher(@NotBlank(message = "Name can not be empty") String name, String password,
			@NotBlank(message = "Email can not be empty") @Email(message = "Blank or Invalid Email") String email,
			@Past LocalDate dob, int rollno, Role userRole) {
		super();
		this.name = name;
		this.password = password;
		this.email = email;
		this.dob = dob;
		this.rollno = rollno;
		this.userRole = userRole;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public LocalDate getDob() {
		return dob;
	}
	public void setDob(LocalDate dob) {
		this.dob = dob;
	}
	public int getRollno() {
		return rollno;
	}
	public void setRollno(int rollno) {
		this.rollno = rollno;
	}
	public List<Subject> getSubjects() {
		return subjects;
	}
	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}
		
	public Role getUserRole() {
		return userRole;
	}
	public void setUserRole(Role userRole) {
		this.userRole = userRole;
	}
			
		@Override
	public String toString() {
		return "Teacher [id=" + id + ", name=" + name + ", password=" + password + ", email=" + email + ", dob=" + dob
				+ ", rollno=" + rollno + ", userRole=" + userRole + "]";
	}
		//add helper methods : in bi dir association
		public void addSubject(Subject a)
		{
			subjects.add(a);
			a.setSubjectTeacher(this);
		}
		public void removeSubject(Subject a)
		{
			subjects.remove(a);
			a.setSubjectTeacher(null);
		}
}
