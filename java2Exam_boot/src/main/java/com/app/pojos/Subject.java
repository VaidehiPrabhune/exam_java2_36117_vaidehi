package com.app.pojos;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

//tb_subjects
//s_id(PK),s_name,s_duration,s_course,t_id(FK)
@Entity
@Table(name = "tb_subjects")
public class Subject {
	@Id //PK 
	@GeneratedValue(strategy = GenerationType.IDENTITY) //strategy = AUTO will be replaced : auto_increment
	@Column(name = "s_id")
	private int id;	
	
	@NotBlank(message = "Name can not be empty")
	@Column(name="s_name")
	private String name;	
	
	@Column(name="s_duration")
	private int duration;	
	
	@Column(name="s_course")
	private String course;	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="t_id",nullable = false)
	private Teacher subjectTeacher;
	
	public Subject(@NotBlank(message = "Name can not be empty") String name, int duration, String course) {
		super();
		this.name = name;
		this.duration = duration;
		this.course = course;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public Teacher getSubjectTeacher() {
		return subjectTeacher;
	}

	public void setSubjectTeacher(Teacher subjectTeacher) {
		this.subjectTeacher = subjectTeacher;
	}

	@Override
	public String toString() {
		return "Subject [id=" + id + ", name=" + name + ", duration=" + duration + ", course=" + course + "]";
	}
	
	
}
