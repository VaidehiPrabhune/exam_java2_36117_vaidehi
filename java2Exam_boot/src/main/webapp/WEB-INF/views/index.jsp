<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1> Welcome to Spring MVC via Spring Framework  @ ${requestScope.server_timestamp}</h1>
	
	<h3> <a href="<spring:url value="/user/login"/>"> User Login </a> </h3>
</body>
</html>