<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h3> ${requestScope.message}</h3>
<h4> Admin Details : ${sessionScope.user_details}</h4>


<table style="background-color: cyan; margin: auto;" border="1">
	<caption>Teachers List</caption>
		<tr>
			<th></th>
			<th> Name </th>
			<th> Email </th>
			<th> Date of birth</th>
			<th> roll no</th>
			<th></th>
			<th></th>
		</tr>
	<c:forEach var="v" items="${requestScope.teacher_list}">
		<tr>
			<td>${v.name}</td>
			<td>${v.email}</td>
			<td>${v.dob}</td>
			<td>${v.rollno}</td>
	
			<td> <a href="<spring:url value='/admin/update?id=${v.id}'/>"> Update </a> </td>
			<td> <a href="<spring:url value='/admin/delete?id=${v.id}'/>"> Delete </a> </td>
		</tr>
	</c:forEach>	
</table>
</body>
</html>