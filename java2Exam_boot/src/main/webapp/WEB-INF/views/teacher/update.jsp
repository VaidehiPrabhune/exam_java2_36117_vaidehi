<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>   
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<form:form method="post" modelAttribute="teacher">
		<table style="background-color: cyan; margin: auto;">
			<tr>	
				<td style="display: none;"><form:input type="number" path="id" /></td>
			</tr>
			<tr>								
				<td style="display: none;"><form:input type="email" path="email" /></td>
			</tr>
			<tr>
				<td>Enter Teacher Name</td>
				<td><form:input type="text" path="name" /></td>
				<td> <form:errors path="name"/></td>
			</tr>
			<tr>
				<td>Enter Password</td>
				<td><form:password path="password" /></td>
				<td> <form:errors path="password"/></td>
			</tr>			
			<tr>
				<td style="display: none;"><form:input type="date" path="dob" /></td>			
			</tr>
			
			<tr>
				<td style="display: none;"><form:input type="number" path="rollno" /></td>			
			</tr>
			
			<tr>
				<td><input type="submit" value="update details"/></td>
			</tr>
		</table>
	</form:form>
</body>
</html>