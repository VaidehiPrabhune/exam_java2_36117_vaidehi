<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%-- To enable Form binding : import spring supplied form taglib --%> -->
	<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form:form method="post" modelAttribute="subject">
		<table style="background-color: cyan; margin: auto;">	
					
			<tr>
				<td>Enter subject Name</td>
				<td><form:input type="text" path="name" /></td>
				<td> <form:errors path="name"/></td>
			</tr>		
			<tr>
				<td>Enter duration</td>
				<td><form:input type="number" path="duration" /></td>
				<td> <form:errors path="duration"/></td>
			</tr>
			<tr>
				<td>enter course</td>
				<td><form:input type="text" path="course" /></td>
				<td> <form:errors path="course"/></td>
			</tr>

			<tr>
				<td><input type="submit" value="Register subject"/></td>
			</tr>
		</table>
	</form:form>
</body>
</html>